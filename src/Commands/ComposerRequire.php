<?php

namespace KDA\Dev\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Process\Process;

class ComposerRequire extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:composer:require {package} {--D|dev} {--W|deps}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'require composer package';


    public function __construct(Filesystem $files)
    {
        parent::__construct();
    }


    public function fire()
    {
        return $this->handle();
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $package = $this->argument('package');
        $dev = $this->option('dev') ? '--dev' : NULL;
        $dep = $this->option('deps') ? '--with-all-dependencies' : NULL;
        $opts = [];
        if ($dev) {
            $opts[] = $dev;
        }

        if ($dep) {
            $opts[] = $dep;
        }
        $process = new Process(array_merge(['composer', 'require',], array_merge($opts, [$package])));
        $process->setTimeout(300);
        $process->run(function ($type, $buffer) {

            $this->info($buffer);
        });
    }
}
