<?php

namespace KDA\Dev\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputOption;

class InstallCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install base laravel site';


    public function __construct(Filesystem $files)
    {
        parent::__construct();
    }


    public function fire()
    {
        return $this->handle();
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->confirm('Would you like to install Backpack?', false)) {

            $this->call('kda:composer:require', ['package' => 'backpack/crud']);

            if ($this->confirm('Would you like to install Improved backpack auth?', false)) {
            }

            if ($this->confirm('Would you like to install Dynamic Sidebars?', false)) {
            }
        }

        if ($this->confirm('Would you like to install Debug Bar?', false)) {
            $this->call(
                'kda:composer:require',
                [
                    'package' => 'barryvdh/laravel-debugbar',
                    '--dev' => true
                ]
            );
        }
    }
}
