<?php

namespace KDA\Example\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Example extends Model
{
    use HasFactory;

    protected $fillable = [
        
    ];

    protected $appends = [
        
    ];

    protected $casts = [
       
    ];

   
    protected static function newFactory()
    {
        return  \KDA\Example\Database\Factories\ExampleFactory::new();
    }

}
