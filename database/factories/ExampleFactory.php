<?php

namespace KDA\Sluggable\Database\Factories;

use KDA\Sluggable\Models\SlugCollection;
use Illuminate\Database\Eloquent\Factories\Factory;

class SlugCollectionFactory extends Factory
{
    protected $model = SlugCollection::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
