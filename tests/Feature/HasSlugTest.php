<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Sluggable\Models\Slug;
use KDA\Tests\Models\Post;
use KDA\Sluggable\Models\SlugCollection;
use KDA\Sluggable\Facades\Slug as SlugFacade;
use KDA\Tests\TestCase;

class HasSlugTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function a_post_has_a_slug()
  {

    $p = Post::create([
      'title' => 'Hello world'
    ]);

    $this->assertEquals($p->slugs->count(), 1);
    $this->assertEquals($p->slugs->first()->slug, 'hello_world');
    $this->assertEquals($p->slugs->first()->getFullPath(), 'posts/hello_world');
  }



  /** @test */
  function retrieve_collection()
  {

    Post::factory()
      ->count(20)->create();

    $posts = Post::get();
    $this->assertEquals(20, $posts->count());


    $collection = SlugFacade::collection('posts');

    $this->assertEquals(20, $collection->slugs->count());
  }


  /** @test */
  function get_by_path()
  {

    $p = Post::create([
      'title' => 'Hello world'
    ]);

    $expectedPath = 'posts/hello_world';

    $this->assertEquals($p->slugs->first()->slug, 'hello_world');
    $this->assertEquals($p->slugs->first()->getFullPath(), $expectedPath);

    $this->assertEquals((SlugFacade::getFirstByPath($expectedPath))->id, $p->id);
  }
}
