<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Sluggable\Models\Slug;
use KDA\Tests\Models\Post;
use KDA\Sluggable\Models\SlugCollection;
use KDA\Sluggable\Facades\Slug as SlugFacade;
use KDA\Tests\TestCase;

class ConfigTest extends TestCase
{
  use RefreshDatabase;



  /** @test */
  function two_same_slug_fails()
  {
    $this->expectException(\Illuminate\Database\QueryException::class);
    $p1 = Post::create([
      'title' => 'Hello world'
    ]);
    $p2 = Post::create([
      'title' => 'Hello world'
    ]);
  }

  /** @test */
  function two_same_slug_increments()
  {
    config(['kda.sluggable.forbid_identical_slug' => false]);
    $p1 = Post::create([
      'title' => 'Hello world'
    ]);
    $p2 = Post::create([
      'title' => 'Hello world'
    ]);

    $this->assertEquals($p1->slugs->count(), 1);
    $this->assertEquals($p2->slugs->count(), 1);
  }

  /** @test */
  function two_same_slug_increment_misconfig()
  {
    $this->expectException(\Illuminate\Database\QueryException::class);
    config(['kda.sluggable.forbid_identical_slug' => false]);
    config(['kda.sluggable.identical_slug_strategy' => 'random']);
    $p1 = Post::create([
      'title' => 'Hello world'
    ]);
    $p2 = Post::create([
      'title' => 'Hello world'
    ]);
  }
}
